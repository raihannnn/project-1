const dataMakanan =[
    {
        gambar:"img/kebab.jpg",
        nama  :"kebab mini sapi/ayam isi 10",
        harga :"Rp.30000"
    
    },

    {
        gambar:"img/donut.jpg",
        nama  :"donat (beraneka rasa)",
        harga :"Rp.15000"
    
    },

    {
        gambar:"img/cireng.jpg",
        nama  :"cireng crispy isi 20",
        harga :"Rp.10000"
    
    },

    {
        gambar:"img/dimsum.jpg",
        nama  :"dimsum isi 9",
        harga :"Rp.24000"
    
    },

    {
        gambar:"img/cuanki.jpg",
        nama  :"baso cuanki",
        harga :"Rp.7500"
    
    },

    {
        gambar:"img/kripiksingkong.jpg",
        nama  :"kripik singkong(beraneka rasa)",
        harga :"Rp.10000"
    
    },

    {
        gambar:"img/singkongthai.jpg",
        nama  :"kripik singkong thailand",
        harga :"Rp.15000"
    
    },

    {
        gambar:"img/pisangluber.jpg",
        nama  :"pisang luber",
        harga :"Rp.13000"
    
    },

    {
        gambar:"img/takoyaki.jpg",
        nama  :"takoyaki isi 9",
        harga :"Rp.17500"
    
    },

    {
        gambar:"img/cilok.jpg",
        nama  :"cilok uhuy",
        harga :"Rp.5000"
    
    },
];

const card = document.querySelector(".card-produk");
for(let data of dataMakanan){
    card.innerHTML += 
    `<div class="container">
    <div class="row">
      <div class="col-lg-4">
        
      </div>
    </div>
  </div>
  <div class="menu-makanan">
    <img src="${data.gambar}" style="height:200px; width:300px;">
         <h5 class="card-title">${data.nama}</h5>
        <p class="card-text">${data.harga}</p>
        <a href="#" class="btn">Pesan</a>

    </div>
  </div>`;
}

